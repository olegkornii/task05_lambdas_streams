package StreamTask;

import java.util.List;
import java.util.OptionalDouble;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Streams {


    public void average() {
        Stream<Integer> stream = Stream.generate(() -> (int) (Math.random() * 20)).limit(20);
        int sum;
        final OptionalDouble average;
        List<Integer> list = stream.collect(Collectors.toList());

        Stream<Integer> showStream = list.stream();
        System.out.print("Random numbers : ");
        showStream.forEach(v -> System.out.print(v + "; "));

        Stream<Integer> sumStream = list.stream();
        sum = sumStream.mapToInt(Integer::intValue).sum();

        Stream<Integer> averageStream = list.stream();
        average = averageStream.mapToInt(Integer::intValue).average();
        double ave = average.getAsDouble();

        Stream<Integer> biggerThanAverage = list.stream().filter(integer -> integer > (int) (ave));
        System.out.print("\nBigger than average : ");
        biggerThanAverage.forEach(v -> System.out.print(v + "; "));

        System.out.println("\nSum = " + sum + " Average = " + ave);

    }


}
