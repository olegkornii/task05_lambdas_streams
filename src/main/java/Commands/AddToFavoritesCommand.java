package Commands;
/**
 * Concrete command
 * Represent add to favorites browser command
 * Implemented as object of command class
 */
public class AddToFavoritesCommand implements Command{
    private Browser b;

    public AddToFavoritesCommand(Browser b) {
        this.b = b;
    }

    @Override
    public void execute() {
        b.addToFavorites();
    }
}
