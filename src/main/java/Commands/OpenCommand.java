package Commands;
/**
 * Concrete command
 * Represent open browser command
 * implemented as lambda
 */
public class OpenCommand  {
    private Browser browser;

    public OpenCommand(Browser browser) {
        this.browser = browser;
    }


    public void execute() {
        Command command = () -> browser.open();
        command.execute();
    }
}
