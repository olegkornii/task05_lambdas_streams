package Commands;

/**
 * Concrete command
 * Represent close browser command
 * Implemented with lambda reference
 */
public class CloseCommand  {
    private static Browser browser;

    public CloseCommand(Browser browser) {
        this.browser = browser;
    }
    private static void executeCommand(){
        browser.close();
    }


    public void execute() {
        Command command = CloseCommand ::executeCommand;
        command.execute();
    }
}
