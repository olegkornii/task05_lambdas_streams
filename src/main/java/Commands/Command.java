package Commands;
@FunctionalInterface
public interface Command {
     void execute();
}
