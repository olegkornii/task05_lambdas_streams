package Commands;

/**
 * Invoker
 * Represent browser user
 * User can invoke all four commands of browser
 */
public class User {
    private Command open;
    private Command close;
    private Command newTab;
    private Command addToFavorites = new AddToFavoritesCommand(new Browser());

//    public User(Command open, Command close, Command newTab, Command addToFavorites) {
//        this.open = open;
//        this.close = close;
//        this.newTab = newTab;
//        this.addToFavorites = addToFavorites;
//    }

    public void openBrowser(){
        OpenCommand openCommand = new OpenCommand(new Browser());
        openCommand.execute();
    }
    public void closeBrowser(){
        CloseCommand close = new CloseCommand(new Browser());
        close.execute();
    }
    public void newTabInBrowser(){
       NewTabCommand newTab = new NewTabCommand(new Browser());
       newTab.execute();
    }
    public void addToFavoritesInBrowser(){
        addToFavorites.execute();
    }
}
