package Commands;
/**
 * Concrete command
 * Represent new tab browser command
 * Implemented as anonymous class
 */
public class NewTabCommand {
    private Browser browser;

    public NewTabCommand(Browser browser) {
        this.browser = browser;
    }


    public void execute() {
        Command command = new Command() {
            @Override
            public void execute() {
                browser.newTab();
            }
        };
        command.execute();
    }
}
