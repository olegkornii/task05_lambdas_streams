package Commands;

/**
 * Receiver
 * Represent browser with 4 methods
 * Open, close, new tab and add to favorites
 */
public class Browser {
    void open(){
        System.out.println("Browser was opened");
    }
    void close(){
        System.out.println("Browser was closed");
    }
    void newTab(){
        System.out.println("Was created new tab");
    }
    void addToFavorites(){
        System.out.println("Added to favorites");
    }
}
