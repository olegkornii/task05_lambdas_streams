@FunctionalInterface
interface MaxValue {
    int maxValue(int x, int y, int z);
}
