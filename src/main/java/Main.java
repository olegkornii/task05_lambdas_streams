import Commands.*;
import StreamTask.Streams;

import java.util.Arrays;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Streams streams = new Streams();
        streams.average();
        Browser b = new Browser();

        User user = new User();
        user.openBrowser();
        user.closeBrowser();
        user.addToFavoritesInBrowser();
        user.newTabInBrowser();
        max();
    }

    private static void max(){
        MaxValue maxValue = Main::findMaxValue;
        Scanner scanner = new Scanner(System.in);
        System.out.println("x = ");
        int x = scanner.nextInt();
        System.out.println("y = ");
        int y = scanner.nextInt();
        System.out.println("z = ");
        int z = scanner.nextInt();
        System.out.println(maxValue.maxValue(x,y,z));

    }

    private static int findMaxValue(int x, int y, int z) {
        int[] arr = {x, y, z};
        Arrays.sort(arr);
        return arr[arr.length - 1];
    }
}
